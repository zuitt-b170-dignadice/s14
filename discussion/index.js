// one line comment ctrl + slash

/* Multiline comment */

console.log('hello batch 170');

/* shift alt a */

/* Javascript we can see the messages/log in the console
browser cosoles are part of the browsers which will allow us to see/log messages, 
or infromation from the programming language

they are easily accessed in the dev tools*/

/* 
    Statements
        instructions, expressions that we add to 
        the programming lagnuage to communicate with 
        the computers

    Usually edning with a semicolon (;).
        However, JS has implemented a way to automatically
        add semicolons at the end of the line

    Syntax 
        set of rules that describes how statements should be made/cosntructed
        
    Lines/blocks of codes must follow certain set of rules for it to work properly.



*/

console.log('Jherson Dignadice')

/* 
Create three console logs to display your favorite food
    console.log('favoritefood')
*/
console.log('adobo');
console.log('Tinola');
console.log('Kwekwek');


let food1 = "adobo";
let food2 = "karekare";
let food3;
food3 = "chimken";

console.log(food1);

/* 
    Variables are a way to store our information or data within the JS

    to create a variable, we first declare the mame of the variable with either let/const keyword.

        let (nameofVariable)

    Then we can now initialize(define) the variable with a value or data

        let (nameofVariable) = (valueofVariable)

*/

console.log('my favorite food is: ' + food1);
console.log('my favorite food is: ' + food1 + ' and ' + food2);
console.log(food3);

/* 
we can create variable without values but the variables content would log undefined
*/


/* 
we can update the content of a variable by reassigning the value using an assignmetn operator (=);

assignament operator lets us reassign values to a variable
*/

 let food4;

 console.log(food4);

 food4 = "Lomi";

 console.log(food4);




 /* 
 we cannot create another variable with the same name. it will result into an error 
 */

 /* 
 let food1 = 'Flat Tops';

 console.log(food1);
 */


 const pi = 3.1416;
 
 console.log(pi);

//  const keyword will also allow creation of variable. However, with a const keyword, we create constant variable, which means the data saved in these variables will not change, cannot change and should not be changed.

 /* 
 
 pi = 'pizza';


 console.log(pi);

 */

//  let, vbar- reassingable
// const - non reassginable

/*  

we also cannot create a const variable without initialization or assigning its value.

const gravity;

console.log(gravity);


*/


/* 

Mini Activity 
    let keyword
        reassign a new value for food1 with another favorite food of yours.

        reassign a new value for food1 with another favorite food of yours.

        log the valused of both values in the console

    const keyword
        create a const variable called sunriseDirection with east as its value

        create a const variable called sunsetDirection with west as its value.

        log the values of both variables in th console     

*/

food1 = "pares";
food2 = "ramen";

console.log(food1);
console.log(food2);

const sunriseDirection = "East";
const sunsetDirection = "West";

console.log(sunriseDirection);
console.log(sunsetDirection);


/* 

    Guideline in creating a JS variable

    1. We can create a let variable with the let keyword. let variables can be reassigned but not redeclared.

    2. creating a variable has two parts.

        declaration of the variable name

        initialization of the initial value of the variable through the use of assignment operator (=)

    3. A let variable can be declared without initializaiton. However, the value of the variable will be undefined until it is re-assigned with a value.

    4. Not defined vs. undefined

        Not defined error means that the variable is used but not DECLARED.

        Undefined results from a variable that is used but not INITIALIZED.

    5. We can used const keyword to create variables. Constant variable cannot be declared without initialization. Constant variables cannot be reassigned.

    6. WHen creating variable names, start with small caps, this is because we are avoiding conflict with JS that is named with captial letters

    7. If the variable would need two word, the naming convention is the use of camelCase. Do not add any space/s for the vatiables with words as names.

*/

// Data types

/* 

    string
        are data types which are alphanumerical text,  they could be a name, phrase, or even a sentence. We can create stringdata types with a single quotee (') or with a double quote ("). The text inisde the quotation marks will be displayed as it is.

    keyword variable = "string data"
    
*/

console.log('sample String Data');

/* 

Numeric Data types 
    are data types which are numeric. numeric data types are displayed when numbers are not placed in the quotation marks. If there is mathematical operation needed to be done, numeric data type could be used isntead of string data type.

Keyword variable = numeric data;

*/

console.log(1234543678);
console.log(1 + 1);

// Mini activity

/* 

using string data type
    display in the console your : 
    name (first and last)
    birthday
    province where you live

using numeric data type
    display in the console your ; 
    high score in the last game you played
    favourite number
    your favourite number in electric fan

*/

console.log('Jherson Dignadice');
console.log('February 9 1999');
console.log('Cavite');

console.log(9);
console.log(7);
console.log(1);











