// js functions
/* 

Functions are used to create reusable commands/statements that prevents the dev from typing a bunch of command.

In field, a big number of lines of codes is the normal output;

using functions would save the dev a lot of time and effort in typing th codes that will be used multiple times.


SYNTAX

    function functionName (parameters){
        command/statement
    };

*/

function printStar(){
    console.log('*')
};

printStar();
printStar();
printStar();
printStar();


function sayHello(name){
    console.log('hello ' + name)
    /* 
    Fuctions can also use parameters. these parameters can be defined and a part of the command inside the function. when called, parameters can be replaced with a target value of the dev. 
    
    make sure the value is inside quotations when called.
    */
};

sayHello('jherson');



//  alert function


/* function alertPrint(){
    alert('hello');
    console.log('hello');
};

alertPrint(); */


//Funtion that accepts two numbers and prints the sum

function add(x, y){
    let sum = x + y;
    console.log(sum);
};

add(1, 2);
add(34, 27);
add(8, 20);
// if the number of parameters defined exceeds the needed, the excess would be ignored by js.

// three parameters
// display the fname, lname, age

function printBio(fName, lName, age){
   // console.log('hello ' + fName + ' ' + lName + ' ' + age)

    // using template literals
    // backticks - the symbol on the left side in the 1 in the keyboard, shift + tilde

    // the displayted data in the console will the defined parameter instead of the text inside the curly braces.

    console.log(`hello ${fName} ${lName} ${age}`);

};
printBio('Jherson', 'Dignadice', 23);

/* let a = 5;
let b = 10;
console.log(`Fifteen is ${a + b} and
not ${2 * a + b}.`);
// "Fifteen is 15 and
// not 20."
 */


// return 

function createFullName(fName, mName, lName){
    return `${fName} ${mName} ${lName}`
    // return specidies the value to be given by the function once it is finishing executing. the value can be given to a variable.it only gives value, but does not display then in the console. 
    
    // that's why we also neet to log the variable in the console outside the function
};

let fullName = createFullName('Jherson', 'Ilagan', 'Dignadice');
console.log(fullName);

/* 

String 1
FName
LName
Age

printUserInfo

returnFunction
*/













