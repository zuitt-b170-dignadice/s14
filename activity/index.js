//Acrtivity

/* 
       Activity:
            1. In the S14 folder, create an activity folder, an index.html file inside of it and link the index.js file.
            2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
            3. Create multiple variables that will store the different JavaScript data types containing information relating to user details
            4. Create a function named printUserInfo that will accept the following information:
            - First name
            - Last name
            - Age
            5. The printUserInfo function will print those details in the console as a single string. 
            This function will also print the hobbies and work address.
            6. Create another function named returnFunction that will return a value and store it in a variable.
            8. Create a git repository named S14.
            9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
            10. Add the link in Boodle.
*/

console.log("Hello World");
// Work Addresss
function work_address_details(houseNumber, street, city, state){
    return `{houseNumber: "${houseNumber}" street: "${street}" city: "${city}" state: "${state}"}`
};

let work_address = work_address_details(32, 'Washington', 'Lincoln', 'Nebraska');


//Hobbies
function hobbies_details(hobby1, hobby2, hobby3){
    return `(3) ["${hobby1}", "${hobby2}", "${hobby3}']`
  
};

let hobbies = hobbies_details('Playing Guitar', 'Online Games', 'Watching Movies');


// General Fuction



function printUserInfo(fName, lName, age, hobbies, work_address){


    console.log(`First Name: ${fName}`);
    console.log(`Last Name: ${lName}`);
    console.log(`Age: ${age}`);
    console.log('Hobbies:')
    console.log(hobbies);
    console.log('Work Address: ')
    console.log(work_address);
    console.log(`${fName} ${lName} is ${age} years of age.`);
    console.log('This was printed inside of the function');
    console.log(hobbies);
    console.log(work_address);

}

printUserInfo('John', 'Smith', '30', hobbies, work_address);


var single = true
var married = false

if (single){
    console.log("The value of is Married is: " + single)
}


